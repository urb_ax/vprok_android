package com.appcraft.weather.domain.global.interactor

class UseCaseResult<out R>(
    val data: R? = null,
    val error: Exception? = null
) {
    override fun toString(): String {
        return error?.let {
            "Error[exception=$error]"
        } ?: run {
            "Success[data=$data]"
        }
    }

    fun process(
        onSuccessCallback: (R) -> Unit,
        onErrorCallback: ((Exception) -> Unit)? = null,
    ) {
        error?.let {
            onErrorCallback?.invoke(it)
        } ?: run {
            data?.let {
                onSuccessCallback(it)
            } ?: run {
                onErrorCallback?.invoke(
                    Exception("Unexpected exception in UseCaseResult: data=null and error=null")
                )
            }
        }
    }
}