package com.appcraft.weather.domain.global

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

class CoroutineProvider {
    val scopeIo = CoroutineScope(Dispatchers.IO)
    val scopeDefault = CoroutineScope(Dispatchers.Default)
    val scopeMain = CoroutineScope(Dispatchers.Main)
    val scopeMainImmediate = CoroutineScope(Dispatchers.Main.immediate)
}
