package com.appcraft.weather.domain.model

data class PageParams(
    val limit: Int,
    val offset: Int
)
