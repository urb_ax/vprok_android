package com.appcraft.weather.domain.model

data class WeatherForecast(
    val cityId: Long,
    val timestamp: Long,
    val temperatureMorning: Float,
    val temperatureDay: Float,
    val temperatureEvening: Float,
    val temperatureNight: Float,
    val pressure: Int,
    val humidity: Int,
    val windSpeed: Float,
    val weatherType: String?,
    val weatherDescription: String?,
    val precipitation: Int
)
