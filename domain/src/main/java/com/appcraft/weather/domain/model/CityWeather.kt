package com.appcraft.weather.domain.model

data class CityWeather(
    val city: City,
    val currentWeather: WeatherForecast? = null,
    val forecasts: List<WeatherForecast> = listOf()
)
