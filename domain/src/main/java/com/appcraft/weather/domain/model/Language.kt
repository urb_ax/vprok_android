package com.appcraft.weather.domain.model

enum class Language(
    val id: Int,
    val tag: String,
    val localizedName: String
) {
    RU(1, "ru", "Русский"),
    EN(2, "en", "English");

    companion object {
        fun getValueFromString(string: String): Language {
            return values().firstOrNull { it.tag == string } ?: RU
        }
    }
}