package com.appcraft.weather.domain.interactor.city

import com.appcraft.weather.domain.gateway.CityGateway
import com.appcraft.weather.domain.global.interactor.UseCaseWithParams
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.Position
import kotlinx.coroutines.Dispatchers

class GetCityForPositionUseCase(
    private val cityGateway: CityGateway
) : UseCaseWithParams<Position, City>(Dispatchers.IO) {
    override suspend fun execute(parameters: Position): City =
        cityGateway.getCityForPosition(parameters)
}