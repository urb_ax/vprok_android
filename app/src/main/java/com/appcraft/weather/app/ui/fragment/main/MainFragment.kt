package com.appcraft.weather.app.ui.fragment.main

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.EmptyDividerDecoration
import com.appcraft.weather.app.global.ui.SingleSwipeListener
import com.appcraft.weather.app.global.ui.fragment.BaseFragment
import com.appcraft.weather.app.global.utils.addSystemWindowInsetToMargin
import com.appcraft.weather.app.global.utils.addSystemWindowInsetToPadding
import com.appcraft.weather.app.global.utils.createFastAdapter
import com.appcraft.weather.app.presentation.main.MainPresenter
import com.appcraft.weather.app.presentation.main.MainView
import com.appcraft.weather.app.ui.item.CityForecastItem
import com.appcraft.weather.app.ui.item.DayForecastItem
import com.appcraft.weather.domain.model.CityWeather
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.mikepenz.fastadapter.*
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.OnBindViewHolderListenerImpl
import com.mikepenz.fastadapter.listeners.OnCreateViewHolderListenerImpl
import com.mikepenz.fastadapter.ui.items.ProgressItem
import kotlinx.android.synthetic.main.bottom_dialog_weather.view.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.item_city_forecast.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import moxy.presenter.InjectPresenter
import pro.appcraft.lib.utils.common.setVisible
import java.util.*

class MainFragment : BaseFragment(), MainView {
    @InjectPresenter
    lateinit var presenter: MainPresenter

    private lateinit var singleSwipeListener: SingleSwipeListener

    private lateinit var adapter: FastAdapter<IItem<*>>
    private lateinit var itemAdapter: ItemAdapter<IItem<*>>
    private lateinit var footerAdapter: ItemAdapter<ProgressItem>

    override val contentView: Int = R.layout.fragment_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAdapters()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        content.addSystemWindowInsetToMargin(top = true, bottom = true)
        initRecyclerView()
        initToolbar()
        initViews()
    }

    private fun initViews() {
        swipeRefreshLayout.setOnRefreshListener { presenter.onRefresh() }
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorAccent
        )
        singleSwipeListener = SingleSwipeListener(recyclerView, R.id.swipeLayout)
    }

    private fun initToolbar() {
        layoutToolbar.apply {
            textViewToolbarHeader.setText(R.string.app_name)
            buttonToolbarLeft.setVisible()
            buttonToolbarLeft.setImageResource(R.drawable.ic_search)
            buttonToolbarLeft.setOnClickListener {
                presenter.onOpenCityPicker()
            }
            buttonToolbarRight.setVisible()
            buttonToolbarRight.setImageResource(R.drawable.ic_information)
            buttonToolbarRight.setOnClickListener {
                presenter.onOpenSettings()
            }
        }
    }

    private fun initAdapters() {
        itemAdapter = ItemAdapter()
        footerAdapter = ItemAdapter()
        adapter = createFastAdapter(itemAdapter, footerAdapter)
        adapter.setHasStableIds(true)

        val preloadItemPosition = resources.getInteger(R.integer.preload_item_position)
        adapter.onCreateViewHolderListener = object : OnCreateViewHolderListenerImpl<IItem<*>>() {
            override fun onPostCreateViewHolder(
                fastAdapter: FastAdapter<IItem<*>>,
                viewHolder: RecyclerView.ViewHolder,
                itemVHFactory: IItemVHFactory<*>
            ): RecyclerView.ViewHolder {
                when (viewHolder) {
                    is CityForecastItem.ViewHolder -> viewHolder.itemView.swipeLayout.addSwipeListener(singleSwipeListener)
                }
                return super.onPostCreateViewHolder(fastAdapter, viewHolder, itemVHFactory)
            }
        }
        adapter.onBindViewHolderListener = object : OnBindViewHolderListenerImpl<IItem<*>>() {
            override fun onBindViewHolder(
                viewHolder: RecyclerView.ViewHolder,
                position: Int,
                payloads: List<Any>
            ) {
                super.onBindViewHolder(viewHolder, position, payloads)
                if (position == adapter.itemCount - preloadItemPosition) {
                    recyclerView.post { presenter.onLoadNext() }
                }
            }
        }

        adapter.addEventHooks(
            listOf(
                CityForecastItem.ContentClickEventHook { _, _, _, item ->
                    showWeatherDetailsDialog(item.cityWeather)
                },
                CityForecastItem.DeleteClickEventHook { _, pos, _, item ->
                    when (val vh = recyclerView.findViewHolderForAdapterPosition(pos)) {
                        is CityForecastItem.ViewHolder -> vh.itemView.swipeLayout.close(true)
                    }
                    presenter.onCityWeatherDelete(item.cityWeather)
                }
            )
        )
    }

    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MainFragment.adapter
            addItemDecoration(EmptyDividerDecoration(context, R.dimen.baseline_grid_small, true))
            addOnScrollListener(
                object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        singleSwipeListener.changeSwipeState(null)
                        super.onScrolled(recyclerView, dx, dy)
                    }
                }
            )
        }
    }

    override fun populateData(data: List<CityWeather>) {
        itemAdapter.setNewList(data.map { CityForecastItem(it) })
    }

    override fun showProgress() {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = true }
    }

    override fun hideProgress() {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = false }
    }

    override fun showEmptyView(show: Boolean) {
        if (show) emptyZeroView.show() else emptyZeroView.hide()
    }

    override fun showErrorView(show: Boolean, message: String) {
        if (show) errorZeroView.show() else errorZeroView.hide()
        errorZeroView.setDescription(message)
    }

    override fun showData(show: Boolean) {
        if (show) recyclerView.setVisible()
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = show }
    }

    override fun showPageProgress(show: Boolean) {
        requireView().post {
            footerAdapter.clear()
            if (show) {
                footerAdapter.add(ProgressItem().apply { isEnabled = false })
            }
        }
    }

    private fun showWeatherDetailsDialog(cityWeather: CityWeather) {
        val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.CustomBottomSheetDialog)
        val rootView = View.inflate(requireContext(), R.layout.bottom_dialog_weather, null)
        bottomSheetDialog.setContentView(rootView)
        bottomSheetDialog.setCancelable(true)
        rootView.setOnClickListener { bottomSheetDialog.cancel() }

        cityWeather.city.apply {
            rootView.textViewCityName.text = name
            rootView.textViewDialogInfo.text = String.format(
                Locale.getDefault(),
                "%s (lat: %.2f, lng: %.2f)",
                country,
                lat,
                lng
            )
        }

        val itemAdapter: ItemAdapter<IItem<*>> = ItemAdapter()
        val adapterItems: FastAdapter<IItem<*>> = FastAdapter.with(itemAdapter)
        adapterItems.setHasStableIds(true)
        rootView.recyclerViewDialog.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = adapterItems
            itemAnimator = null
            addItemDecoration(
                EmptyDividerDecoration(
                    requireContext(),
                    R.dimen.baseline_grid_medium,
                    true
                )
            )
        }
        itemAdapter.setNewList(cityWeather.forecasts.map { DayForecastItem(it) })

        val behavior = BottomSheetBehavior.from(rootView.parent as View)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
        behavior.skipCollapsed = true
        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(view: View, state: Int) {
                if (state == BottomSheetBehavior.STATE_HIDDEN) {
                    bottomSheetDialog.cancel()
                }
            }

            override fun onSlide(view: View, v: Float) {}
        })

        bottomSheetDialog.window
            ?.findViewById<View>(com.google.android.material.R.id.container)
            ?.fitsSystemWindows = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            bottomSheetDialog.window?.decorView?.let {
                it.systemUiVisibility = it.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            }
        }
        rootView.layoutDialogContent.addSystemWindowInsetToPadding(bottom = true)

        bottomSheetDialog.show()
    }

    companion object {
        fun newInstance(): Fragment {
            val fragment = MainFragment()
            fragment.arguments = bundleOf()
            return fragment
        }
    }
}