@file:Suppress("PackageDirectoryMismatch")

import android.content.Context
import com.appcraft.weather.data.BuildConfig
import com.appcraft.weather.data.network.ErrorResponseInterceptor
import com.appcraft.weather.data.network.HeaderInterceptor
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

val retrofitModule = module {
    val responsesPath = "responses"
    val cacheMaxSize = 100 * 1024 * 1024 // 100 MB
    val timeout = 30_000L // 30 sec

    fun getCache(context: Context): Cache? {
        var cache: Cache? = null
        try {
            val httpCacheDirectory = File(context.cacheDir, responsesPath)
            cache = Cache(httpCacheDirectory, cacheMaxSize.toLong())
        } catch (ignored: Exception) {
        }

        return cache
    }

    fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) BODY else NONE
        return httpLoggingInterceptor
    }

    factory(named("AuthClient")) {
        OkHttpClient.Builder()
            //.addInterceptor(ChuckerInterceptor(androidContext()))
            .addInterceptor(getHttpLoggingInterceptor())
            .addInterceptor(HeaderInterceptor(get()))
            .addInterceptor(ErrorResponseInterceptor(get()))
            .cache(getCache(androidContext()))
            .callTimeout(timeout, TimeUnit.MILLISECONDS)
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .build()
    }

    factory(named("CommonClient")) {
        OkHttpClient.Builder()
            //.addInterceptor(ChuckerInterceptor(androidContext()))
            .addInterceptor(getHttpLoggingInterceptor())
            .addInterceptor(HeaderInterceptor(get()))
            .addInterceptor(ErrorResponseInterceptor(get()))
            .cache(getCache(androidContext()))
            .callTimeout(timeout, TimeUnit.MILLISECONDS)
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .build()
    }

    fun getBuilder(
        okHttpClient: OkHttpClient
    ): Retrofit.Builder {
        val gson = GsonBuilder().serializeNulls().create()
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    factory(named("CommonBuilder")) {
        getBuilder(okHttpClient = get(named("CommonClient")))
    }

    factory(named("AuthBuilder")) {
        getBuilder(okHttpClient = get(named("AuthClient")))
    }

    single(named("AuthRetrofit")) {
        get<Retrofit.Builder>(named("AuthBuilder"))
            .baseUrl(BuildConfig.API_END_POINT).build()
    }

    single(named("CommonRetrofit")) {
        get<Retrofit.Builder>(named("CommonBuilder"))
            .baseUrl(BuildConfig.API_END_POINT).build()
    }
}
