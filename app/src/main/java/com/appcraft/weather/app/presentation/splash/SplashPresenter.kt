package com.appcraft.weather.app.presentation.splash

import com.appcraft.weather.app.R
import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.presentation.BasePresenter
import com.appcraft.weather.domain.global.CoroutineProvider
import com.appcraft.weather.domain.interactor.city.InitCitiesUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import moxy.InjectViewState
import org.koin.core.component.inject

@InjectViewState
class SplashPresenter : BasePresenter<SplashView>() {
    private val coroutineProvider: CoroutineProvider by inject()
    private val notifier: Notifier by inject()
    private val initCitiesUseCase: InitCitiesUseCase by inject()

    private var uiReady: Boolean = false
    private var dbReady: Boolean = false

    init {
        initDb()
    }

    private fun initDb() {
        coroutineProvider.scopeDefault.launch {
            initCitiesUseCase().process(
                {
                    dbReady = true
                    tryToContinue()
                },
                {
                    notifier.sendMessage(R.string.unknown_error)
                    coroutineProvider.scopeDefault.launch {
                        delay(3000)
                        viewState.routerFinishFlow()
                        viewState.routerFinishFlow()
                    }
                }
            )
        }
    }

    fun onAnimationEnd() {
        uiReady = true
        tryToContinue()
    }

    private fun tryToContinue() {
        if (uiReady) {
            if (dbReady) {
                viewState.routerNewRootFlow(Screens.MainFlow())
            } else {
                viewState.showDataBaseInitProgress()
            }
        }
    }
}
