@file:Suppress("PackageDirectoryMismatch")

import androidx.preference.PreferenceManager
import com.appcraft.weather.data.preference.Preferences
import com.f2prateek.rx.preferences2.RxSharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val preferenceModule = module {
    single { Preferences(get()) }
    factory { RxSharedPreferences.create(get()) }
    factory { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
}
