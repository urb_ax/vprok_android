package com.appcraft.weather.app.global.utils

import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat

fun View.setBackgroundTintList(@ColorRes res: Int) {
    this.context?.let { this.backgroundTintList = ContextCompat.getColorStateList(it, res) }
}

fun AppCompatImageView.setImageTintList(@ColorRes res: Int) {
    this.context?.let { this.imageTintList = ContextCompat.getColorStateList(it, res) }
}

fun View.getPosition(): IntArray {
    return IntArray(2).also { this.getLocationOnScreen(it) }
}

fun TextView.setTextColorRes(@ColorRes res: Int) {
    this.context?.let { this.setTextColor(ContextCompat.getColor(it, res)) }
}