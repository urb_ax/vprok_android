package com.appcraft.weather.app.global.pagination

import com.appcraft.weather.domain.global.interactor.UseCaseResult
import kotlinx.coroutines.CoroutineScope
import kotlin.reflect.KSuspendFunction2

class OffsetPaginator<T: Any>(
    coroutineScope: CoroutineScope,
    private val requestFactory: KSuspendFunction2<Int, Int, UseCaseResult<List<T>>>,
    viewController: ViewController<T>,
    limit: Int = DEFAULT_LIMIT
) : BasePaginator<T>(coroutineScope, viewController, limit) {
    @Throws(Exception::class)
    override suspend fun loadRequest(currentData: List<T>?, limit: Int): UseCaseResult<List<T>> {
        val offset = currentData?.size ?: ZERO_OFFSET

        return requestFactory(offset, limit)
    }

    companion object {
        private const val ZERO_OFFSET = 0
    }
}