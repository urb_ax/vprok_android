@file:Suppress("PackageDirectoryMismatch", "USELESS_CAST")

import com.appcraft.weather.app.global.event.EventDispatcher
import pro.appcraft.lib.navigation.AppRouter
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone

val navigationModule = module {
    val cicerone: Cicerone<AppRouter> = Cicerone.create(AppRouter())
    single { cicerone.router as AppRouter }
    single { cicerone.navigatorHolder }
    single { EventDispatcher() }
}
