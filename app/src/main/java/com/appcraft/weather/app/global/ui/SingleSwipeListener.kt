package com.appcraft.weather.app.global.ui

import androidx.annotation.IdRes
import androidx.core.view.iterator
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout

class SingleSwipeListener(
    var recyclerView: RecyclerView,
    @IdRes var swipeRes: Int
) : SwipeLayout.SwipeListener {
    override fun onOpen(layout: SwipeLayout?) {
        layout?.let { changeSwipeState(it) }
    }

    override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {}

    override fun onStartOpen(layout: SwipeLayout?) {
        layout?.let { changeSwipeState(it) }
    }

    override fun onStartClose(layout: SwipeLayout?) {}

    override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {}

    override fun onClose(layout: SwipeLayout?) {}

    fun changeSwipeState(layout: SwipeLayout?) {
        recyclerView.iterator().forEach {
            it.findViewById<SwipeLayout>(swipeRes)?.let { swipeLayout ->
                if (swipeLayout != layout && swipeLayout.openStatus == SwipeLayout.Status.Open) {
                    swipeLayout.close(true)
                }
            }
        }
    }
}