package com.appcraft.weather.app

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.fragment.app.Fragment
import com.appcraft.weather.app.ui.fragment.main.MainFlowFragment
import com.appcraft.weather.app.ui.fragment.main.SettingsFragment
import com.appcraft.weather.app.ui.fragment.main.CityPickerFragment
import com.appcraft.weather.app.ui.fragment.main.MainFragment
import com.appcraft.weather.app.ui.fragment.splash.SplashFlowFragment
import com.appcraft.weather.app.ui.fragment.splash.SplashFragment
import pro.appcraft.lib.navigation.AppScreen

class Screens {
    class SplashFlow : AppScreen() {
        override fun getFragment(): Fragment = SplashFlowFragment.newInstance()
    }

    class SplashScreen : AppScreen() {
        override fun getFragment(): Fragment = SplashFragment.newInstance()
    }

    class MainFlow : AppScreen() {
        override fun getFragment(): Fragment = MainFlowFragment.newInstance()
    }

    class MainScreen : AppScreen() {
        override fun getFragment(): Fragment = MainFragment.newInstance()
    }

    class CityPickerScreen(
        private val requestId: Long
    ) : AppScreen() {
        override fun getFragment(): Fragment = CityPickerFragment.newInstance(requestId)
    }

    class SettingsScreen : AppScreen() {
        override fun getFragment(): Fragment = SettingsFragment.newInstance()
    }


    // STANDART ACTIONS

    @Suppress("unused")
    class AppSettingsActionScreen : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val myAppSettings = Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + context.packageName)
            )
            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
            myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            return myAppSettings
        }
    }

    @Suppress("unused")
    class ActionOpenLinkScreen(
        private val link: String
    ) : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(link)
            return intent
        }
    }

    @Suppress("unused")
    class ActionOpenDialScreen(private val phone: String) : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.fromParts("tel", phone, null)
            return intent
        }
    }

    @Suppress("unused")
    class ActionMailToScreen(
        private val email: String,
        private val subject: String
    ) : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)

            return Intent.createChooser(
                intent,
                context.resources.getString(R.string.write_on_email)
            )
        }
    }

    @Suppress("unused")
    class ActionShareTextScreen(
        private val text: String,
        private val header: String? = null
    ) : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, text)
            intent.putExtra("sms_body", text)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
            intent.type = "text/plain"

            return Intent.createChooser(
                intent,
                header ?: context.resources.getString(R.string.share)
            )
        }
    }

    @Suppress("unused")
    class ActionOpenFolderScreen(
        private val uri: Uri
    ) : AppScreen() {
        override fun getActivityIntent(context: Context): Intent? {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, "*/*")
            return Intent.createChooser(
                intent,
                context.resources.getString(R.string.open_folder)
            )
        }
    }
}
