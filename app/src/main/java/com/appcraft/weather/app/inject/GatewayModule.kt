@file:Suppress("PackageDirectoryMismatch", "USELESS_CAST")

import com.appcraft.weather.data.gateway.*
import com.appcraft.weather.domain.gateway.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val gatewayModule = module {
    single { CityGatewayImpl(androidContext(), get(), get(), get()) as CityGateway }
}
