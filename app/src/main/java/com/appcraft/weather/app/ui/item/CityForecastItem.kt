package com.appcraft.weather.app.ui.item

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.BaseViewHolder
import com.appcraft.weather.domain.model.CityWeather
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.item_city_forecast.view.*
import pro.appcraft.lib.utils.common.setGone
import pro.appcraft.lib.utils.common.setVisible
import java.util.*

class CityForecastItem(
    val cityWeather: CityWeather
) : AbstractItem<CityForecastItem.ViewHolder>() {
    override var identifier = cityWeather.city.id

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override val type = R.id.cityForecastItem

    override val layoutRes = R.layout.item_city_forecast

    class ViewHolder(view: View) : BaseViewHolder<CityForecastItem>(view) {
        override fun bindView(item: CityForecastItem, payloads: List<Any>) {
            itemView.textViewName.text = item.cityWeather.city.name
            item.cityWeather.currentWeather?.apply {
                itemView.textViewTemperature.text = String.format(
                    Locale.getDefault(),
                    "%.1f %s",
                    temperatureDay,
                    getString(R.string.temperature_celsius)
                )
                itemView.textViewInfo.setVisible()
                itemView.textViewInfo.text = String.format(
                    Locale.getDefault(),
                    "%s: %s\n%s: %d%%\n%s: %d %s\n%s: %.1f %s",
                    getString(R.string.weather),
                    weatherDescription,
                    getString(R.string.humidity),
                    humidity,
                    getString(R.string.pressure),
                    pressure,
                    getString(R.string.pressure_mbar),
                    getString(R.string.wind_speed),
                    windSpeed,
                    getString(R.string.m_sec)
                )
            } ?: run {
                itemView.textViewTemperature.text = getString(R.string.data_not_available)
                itemView.textViewInfo.setGone()
            }

            itemView.swipeLayout.isClickToClose = true
            itemView.swipeLayout.isLeftSwipeEnabled = false
        }
    }

    class ContentClickEventHook(listener: ((View, Int, FastAdapter<CityForecastItem>, CityForecastItem) -> Unit)?) :
        BaseClickEventHook({ it.itemView.contentView }, listener)

    class DeleteClickEventHook(listener: ((View, Int, FastAdapter<CityForecastItem>, CityForecastItem) -> Unit)?) :
        BaseClickEventHook({ it.itemView.deleteContainer }, listener)

    open class BaseClickEventHook(
        private val binder: (ViewHolder) -> View,
        private val listener: ((View, Int, FastAdapter<CityForecastItem>, CityForecastItem) -> Unit)?
    ) : ClickEventHook<CityForecastItem>() {
        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? =
            if (viewHolder is ViewHolder) binder.invoke(viewHolder) else super.onBind(viewHolder)

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<CityForecastItem>,
            item: CityForecastItem
        ) {
            listener?.invoke(v, position, fastAdapter, item)
        }
    }
}
