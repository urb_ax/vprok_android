package com.appcraft.weather.app.inject

import androidx.room.Room
import com.appcraft.weather.data.storage.AppDb
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

internal val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDb::class.java,
            "app.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
    factory { get<AppDb>().getCityDao() }
    factory { get<AppDb>().getWeatherForecastDao() }
}