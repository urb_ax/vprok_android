package com.appcraft.weather.app.global.ui.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import pro.appcraft.lib.navigation.AppRouter
import pro.appcraft.lib.navigation.AppScreen
import pro.appcraft.lib.navigation.FlowCiceroneViewModel
import pro.appcraft.lib.navigation.FlowRouter
import pro.appcraft.lib.utils.common.hideKeyboard

abstract class BaseFragment : MvpAppCompatFragment() {
    private val disposables = CompositeDisposable()
    open var isLightStatusBar: Boolean = true
        protected set
    open var isLightNavigationBar: Boolean = true
        protected set

    val appRouter: AppRouter by inject()

    protected val router: FlowRouter?
        get() {
            val flowParent = getParent(this) ?: return null

            return ViewModelProvider(flowParent)
                .get(FlowCiceroneViewModel::class.java)
                .getFlowCicerone(appRouter).router
        }

    private fun getParent(fragment: Fragment): FlowFragment? {
        return when {
            fragment is FlowFragment -> fragment
            fragment.parentFragment == null -> null
            else -> getParent(fragment.requireParentFragment())
        }
    }

    @get:LayoutRes
    protected abstract val contentView: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(contentView, container, false)
    }

    override fun onDestroyView() {
        hideKeyboard()
        super.onDestroyView()
    }

    protected fun unsubscribeOnDestroy(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    open fun onBackPressed() {
        router?.exit()
    }

    override fun onResume() {
        super.onResume()
        initStatusAndNavigationBar()
    }

    private fun initStatusAndNavigationBar() {
        if (this is FlowFragment || childFragmentManager.fragments.isNotEmpty()) {
            return
        }
        updateSystemBarsColors()
    }

    fun updateSystemBarsColors() {
        applyStatusBarMode()
        applyNavigationBarMode()
    }

    fun setStatusBarMode(isLightStatusBar: Boolean) {
        this.isLightStatusBar = isLightStatusBar
        applyStatusBarMode()
    }

    private fun applyStatusBarMode() {
        var flags = requireActivity().window.decorView.systemUiVisibility

        flags = if (isLightStatusBar) {
            flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }

        requireActivity().window.decorView.systemUiVisibility = flags
    }

    private fun applyNavigationBarMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity?.apply {
                var flags = window.decorView.systemUiVisibility
                flags = if (isLightNavigationBar) {
                    flags or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                } else {
                    flags and View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR.inv()
                }
                window.decorView.systemUiVisibility = flags
            }
        }
    }

    fun routerNavigateTo(screen: AppScreen) {
        router?.navigateTo(screen)
    }

    fun routerNewRootScreen(screen: AppScreen) {
        router?.newRootScreen(screen)
    }

    fun routerNewRootChain(vararg screens: AppScreen) {
        router?.newRootChain(*screens)
    }

    fun routerExit() {
        router?.exit()
    }

    fun routerStartFlow(flow: AppScreen) {
        router?.startFlow(flow)
    }

    fun routerReplaceFlow(flow: AppScreen) {
        router?.replaceFlow(flow)
    }

    fun routerNewRootFlow(flow: AppScreen) {
        router?.newRootFlow(flow)
    }

    fun routerNewRootFlowChain(vararg flows: AppScreen) {
        router?.newRootFlowChain(*flows)
    }

    fun routerFinishFlow() {
        router?.finishFlow()
    }

    fun routerBackTo(flow: AppScreen) {
        router?.backTo(flow)
    }

    fun routerForwardTo(flow: AppScreen) {
        router?.forwardTo(flow)
    }

    fun routerToTop(flow: AppScreen) {
        router?.toTop(flow)
    }
}
