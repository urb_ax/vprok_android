package com.appcraft.weather.app.global.ui.activity

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.android.ext.android.inject
import com.appcraft.weather.app.global.ui.fragment.BaseFragment
import com.appcraft.weather.data.preference.Preferences
import java.util.*
import android.os.LocaleList
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.presentation.ErrorHandler
import com.appcraft.weather.domain.model.Language
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow

abstract class BaseActivity : MvpAppCompatActivity() {
    private val preferences: Preferences by inject()
    private val errorHandler: ErrorHandler by inject()

    private val disposables = CompositeDisposable()

    private fun getCurrentFragment(): BaseFragment? {
        return supportFragmentManager.findFragmentById(R.id.container) as? BaseFragment
    }

    protected fun Disposable.unsubscribeOnDestroy() {
        disposables.add(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setLanguage()
        super.onCreate(savedInstanceState)
    }

    @Suppress("DEPRECATION")
    private fun setLanguage() {
        if (!preferences.language.isSet()) {
            setDefaultLanguage()
        }

        val locale = Locale(preferences.language.get())
        Locale.setDefault(locale)
        val baseConfiguration = baseContext.resources.configuration
        val appConfiguration = applicationContext.resources.configuration
        baseConfiguration.setLocale(locale)
        appConfiguration.setLocale(locale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val localeList = LocaleList(locale)
            LocaleList.setDefault(localeList)
            baseConfiguration.setLocales(localeList)
            appConfiguration.setLocales(localeList)

            baseContext.createConfigurationContext(baseConfiguration)
            applicationContext.createConfigurationContext(appConfiguration)
        } else {
            baseContext.resources
                .updateConfiguration(baseConfiguration, baseContext.resources.displayMetrics)
            applicationContext.resources
                .updateConfiguration(appConfiguration, applicationContext.resources.displayMetrics)
        }

        errorHandler.resources = baseContext.resources
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(updateBaseContextLocale(base))

        errorHandler.resources = baseContext.resources
    }

    private fun updateBaseContextLocale(context: Context?): Context? {
        if (!preferences.language.isSet()) {
            setDefaultLanguage()
        }

        val locale = Locale(preferences.language.get())
        Locale.setDefault(locale)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, locale)
        }

        return updateResourcesLocaleLegacy(context, locale)
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResourcesLocale(context: Context?, locale: Locale): Context? {
        context?.let {
            val configuration = context.resources.configuration
            configuration.setLocale(locale)
            return context.createConfigurationContext(configuration)
        }
        return null
    }

    @Suppress("DEPRECATION")
    private fun updateResourcesLocaleLegacy(context: Context?, locale: Locale): Context? {
        context?.let {
            val resources = context.resources
            val configuration = resources.configuration
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.displayMetrics)
            return context
        }
        return null
    }

    private fun setDefaultLanguage() {
        preferences.language.set(
            Language.getValueFromString(Locale.getDefault().language).tag
        )
    }

    public override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    override fun onBackPressed() {
        val currentFragment = getCurrentFragment()
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}
