@file:Suppress("PackageDirectoryMismatch")

import com.appcraft.weather.app.inject.databaseModule
import org.koin.core.module.Module

val appComponent: List<Module> = listOf(
    apiModule,
    appModule,
    retrofitModule,
    gatewayModule,
    interactorModule,
    navigationModule,
    preferenceModule,
    databaseModule
)