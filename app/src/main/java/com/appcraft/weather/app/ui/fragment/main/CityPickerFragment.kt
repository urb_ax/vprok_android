package com.appcraft.weather.app.ui.fragment.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appcraft.weather.app.R
import com.appcraft.weather.app.Screens
import com.appcraft.weather.app.global.notifier.Notifier
import com.appcraft.weather.app.global.ui.fragment.BaseFragment
import com.appcraft.weather.app.global.utils.addSystemWindowInsetToMargin
import com.appcraft.weather.app.global.utils.createFastAdapter
import com.appcraft.weather.app.presentation.citypicker.CityPickerPresenter
import com.appcraft.weather.app.presentation.citypicker.CityPickerView
import com.appcraft.weather.app.ui.item.CitySearchItem
import com.appcraft.weather.domain.model.City
import com.appcraft.weather.domain.model.Position
import com.google.android.gms.location.*
import com.mikepenz.fastadapter.ClickListener
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.OnBindViewHolderListenerImpl
import com.mikepenz.fastadapter.ui.items.ProgressItem
import kotlinx.android.synthetic.main.fragment_city_picker.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import moxy.presenter.InjectPresenter
import org.koin.android.ext.android.inject
import pro.appcraft.lib.permissions.LocationPermissionHandler
import pro.appcraft.lib.utils.common.setGone
import pro.appcraft.lib.utils.common.setVisible
import java.util.*
import kotlin.concurrent.schedule

class CityPickerFragment : BaseFragment(), CityPickerView {
    @InjectPresenter
    lateinit var presenter: CityPickerPresenter
    private val locationPermissionHandler: LocationPermissionHandler by inject()
    private val notifier: Notifier by inject()

    private var queryTask: TimerTask? = null

    private lateinit var adapter: FastAdapter<IItem<*>>
    private lateinit var itemAdapter: ItemAdapter<IItem<*>>
    private lateinit var footerAdapter: ItemAdapter<ProgressItem>

    private val fusedLocationProviderClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(requireContext())
    }
    private var receivedUserPositionsCount = 0
    private var suggestedCityExists = false

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            val location = locationResult?.locations?.find { location -> location != null }
            if (location != null) {
                receivedUserPositionsCount++
                if (receivedUserPositionsCount == USER_POSITION_TO_SEND) {
                    presenter.onSendUserLocation(
                        Position(
                            lat = location.latitude,
                            lng = location.longitude
                        )
                    )
                    fusedLocationProviderClient.removeLocationUpdates(this)
                    receivedUserPositionsCount = 0
                }
            }
        }
    }

    override val contentView: Int = R.layout.fragment_city_picker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAdapters()

        arguments?.getLong(REQUEST_ID)?.let {
            presenter.setParams(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        content.addSystemWindowInsetToMargin(top = true, bottom = true)
        initRecyclerView()
        initToolbar()
        initViews()
        initOnClickListeners()
        initEditTextListeners()

        requestLocationData()
    }

    override fun onStop() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }

    private fun initAdapters() {
        itemAdapter = ItemAdapter()
        footerAdapter = ItemAdapter()
        adapter = createFastAdapter(itemAdapter, footerAdapter)
        adapter.setHasStableIds(true)

        val preloadItemPosition = resources.getInteger(R.integer.preload_item_position)
        adapter.onBindViewHolderListener = object : OnBindViewHolderListenerImpl<IItem<*>>() {
            override fun onBindViewHolder(
                viewHolder: RecyclerView.ViewHolder,
                position: Int,
                payloads: List<Any>
            ) {
                super.onBindViewHolder(viewHolder, position, payloads)
                if (position == adapter.itemCount - preloadItemPosition) {
                    recyclerView.post { presenter.onLoadNext() }
                }
            }
        }
        adapter.onClickListener = object : ClickListener<IItem<*>> {
            override fun invoke(v: View?, adapter: IAdapter<IItem<*>>, item: IItem<*>, position: Int): Boolean {
                return if (item is CitySearchItem) {
                    presenter.onCitySelected(item.city)
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@CityPickerFragment.adapter
            itemAnimator = null
        }
    }

    private fun initToolbar() {
        layoutToolbar.apply {
            textViewToolbarHeader.setText(R.string.city_search)
            buttonToolbarLeft.setVisible()
            buttonToolbarLeft.setImageResource(R.drawable.ic_back)
            buttonToolbarLeft.setOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun initViews() {
        swipeRefreshLayout.setOnRefreshListener {
            presenter.onLoadNewList(editTextSearch.text.toString())
        }
        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorAccent
        )
    }

    private fun initEditTextListeners() {
        editTextSearch.addTextChangedListener {
            queryTask?.cancel()
            queryTask = Timer().schedule(QUERY_DELAY_MS) {
                activity?.runOnUiThread {
                    presenter.onLoadNewList(it.toString())
                }
            }
        }
    }

    private fun initOnClickListeners() {
        buttonLocation.setOnClickListener {
            requestLocationData(false)
        }
    }

    override fun populateData(data: List<City>, suggestedCity: City?) {
        val items = mutableListOf<IItem<*>>()
        suggestedCity?.let {
            items.add(CitySearchItem(suggestedCity))
            showEmptyView(false)
        }
        suggestedCityExists = suggestedCity != null
        items.addAll(
            data.mapNotNull {
                if (it.id != suggestedCity?.id) {
                    CitySearchItem(it)
                } else {
                    null
                }
            }
        )
        view?.post {
            itemAdapter.setNewList(items)
        }
    }

    override fun showLocationProgress() {
        progressBarLocation.setVisible()
    }

    override fun hideLocationProgress() {
        progressBarLocation.setGone()
    }

    override fun showEmptyView(show: Boolean) {
        if (show && (!suggestedCityExists)) emptyZeroView.show() else emptyZeroView.hide()
    }

    override fun showErrorView(show: Boolean, message: String) {
        if (show) errorZeroView.show() else errorZeroView.hide()
        errorZeroView.setDescription(message)
    }

    override fun showData(show: Boolean) {
        if (show) recyclerView.setVisible()
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        swipeRefreshLayout.post { swipeRefreshLayout?.isRefreshing = show }
    }

    override fun showPageProgress(show: Boolean) {
        requireView().post {
            footerAdapter.clear()
            if (show) {
                footerAdapter.add(ProgressItem().apply { isEnabled = false })
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationData(silent: Boolean = true) {
        showLocationProgress()
        locationPermissionHandler.checkPermissions(requireActivity()) { permissionGranted ->
            if (permissionGranted) {
                val locationRequest = LocationRequest().apply {
                    interval = 10000
                    fastestInterval = 5000
                    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                }
                fusedLocationProviderClient.removeLocationUpdates(locationCallback)
                fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )
            } else {
                if (!silent) {
                    notifier.sendActionMessage(
                        R.string.location_permission_request,
                        R.string.settings
                    ) {
                        routerStartFlow(Screens.AppSettingsActionScreen())
                    }
                }
                presenter.onLocationAccessDenied()
            }
        }
    }

    companion object {
        private const val REQUEST_ID = "REQUEST_ID"
        private const val QUERY_DELAY_MS = 500L
        private const val USER_POSITION_TO_SEND = 2

        fun newInstance(requestId: Long): Fragment {
            val fragment = CityPickerFragment()
            fragment.arguments = bundleOf(
                REQUEST_ID to requestId
            )
            return fragment
        }
    }
}