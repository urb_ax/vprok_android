package com.appcraft.weather.app.global.presentation

import moxy.MvpPresenter
import moxy.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent

@OptIn(KoinApiExtension::class)
open class BasePresenter<View : MvpView> : MvpPresenter<View>(), KoinComponent {
    private val disposables = CompositeDisposable()

    protected fun Disposable.unsubscribeOnDestroy() {
        disposables.add(this)
    }

    override fun onDestroy() {
        disposables.clear()
    }
}
