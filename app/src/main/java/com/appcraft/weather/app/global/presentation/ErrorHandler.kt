package com.appcraft.weather.app.global.presentation

import android.content.res.Resources
import com.appcraft.weather.app.R
import com.appcraft.weather.data.network.ServerException
import mu.KotlinLogging
import retrofit2.HttpException
import java.io.IOException
import java.net.NoRouteToHostException
import java.net.SocketTimeoutException

private val logger = KotlinLogging.logger {}

class ErrorHandler(
    var resources: Resources
) {
    fun proceed(error: Throwable, callback: (String) -> Unit) {
        logger.error(error.message, error)
        callback(getUserMessage(error))
    }

    private fun getUserMessage(error: Throwable): String {
        return when (error) {
            is IOException -> resources.getString(getIOErrorMessage(error))
            is ServerException -> {
                error.message ?: resources.getString(R.string.unknown_error)
            }
            is HttpException -> resources.getString(getServerErrorMessage(error))
            is SecurityException -> resources.getString(R.string.security_error)
            else -> {
                resources.getString(R.string.unknown_error)
            }
        }
    }

    private fun getIOErrorMessage(error: IOException): Int {
        return if (error is NoRouteToHostException  || error is SocketTimeoutException) {
            R.string.server_not_available_error
        } else {
            R.string.network_error
        }
    }

    private fun getServerErrorMessage(exception: HttpException): Int {
        return when (exception.code()) {
            BAD_REQUEST -> R.string.bad_request_error
            UNAUTHORIZED -> R.string.unauthorized_error
            FORBIDDEN -> R.string.forbidden_error
            NOT_FOUND -> R.string.not_found_error
            INTERNAL_SERVER_ERROR -> R.string.internal_server_error
            else -> R.string.unknown_error
        }
    }

    companion object {
        const val BAD_REQUEST = 400
        const val UNAUTHORIZED = 401
        const val FORBIDDEN = 403
        const val NOT_FOUND = 404
        const val INTERNAL_SERVER_ERROR = 500
    }
}
