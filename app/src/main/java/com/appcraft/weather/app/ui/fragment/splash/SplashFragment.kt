package com.appcraft.weather.app.ui.fragment.splash

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.appcraft.weather.app.R
import com.appcraft.weather.app.global.ui.fragment.BaseFragment
import com.appcraft.weather.app.presentation.splash.SplashPresenter
import com.appcraft.weather.app.presentation.splash.SplashView
import kotlinx.android.synthetic.main.fragment_splash.*
import moxy.presenter.InjectPresenter
import pro.appcraft.lib.utils.common.setVisible

class SplashFragment : BaseFragment(), SplashView {
    override var isLightNavigationBar = true

    @InjectPresenter
    lateinit var presenter: SplashPresenter

    override val contentView: Int = R.layout.fragment_splash

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startFirstAnimation()
        // activate this if splash background color differs from main window background color
//        view.postDelayed(
//            { activity?.window?.setBackgroundDrawableResource(R.color.colorBackground) },
//            ANIMATION_DURATION_MS / 2
//        )
    }

    private fun startFirstAnimation() {
        imageViewLogo.animate()
            .alpha(1f)
            .setStartDelay(0)
            .setDuration(ANIMATION_DURATION_MS)
            .withEndAction {
                presenter.onAnimationEnd()
            }
            .start()
    }

    override fun showDataBaseInitProgress() {
        progressBar.setVisible()
        textViewMessage.setText(R.string.db_cities_init)
        textViewMessage.setVisible()
    }

    companion object {
        private const val ANIMATION_DURATION_MS = 3000L

        fun newInstance(): SplashFragment {
            val fragment = SplashFragment()
            fragment.arguments = bundleOf()
            return fragment
        }
    }
}
