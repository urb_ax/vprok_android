package com.appcraft.weather.app.global.presentation

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import pro.appcraft.lib.navigation.AppScreen

interface NavigationMvpView : MvpView {
    @OneExecution
    fun routerNavigateTo(screen: AppScreen)

    @OneExecution
    fun routerNewRootScreen(screen: AppScreen)

    @OneExecution
    fun routerNewRootChain(vararg screens: AppScreen)

    @OneExecution
    fun routerExit()

    @OneExecution
    fun routerStartFlow(flow: AppScreen)

    @OneExecution
    fun routerReplaceFlow(flow: AppScreen)

    @OneExecution
    fun routerNewRootFlow(flow: AppScreen)

    @OneExecution
    fun routerNewRootFlowChain(vararg flows: AppScreen)

    @OneExecution
    fun routerFinishFlow()

    @OneExecution
    fun routerBackTo(flow: AppScreen)

    @OneExecution
    fun routerForwardTo(flow: AppScreen)

    @OneExecution
    fun routerToTop(flow: AppScreen)
}
