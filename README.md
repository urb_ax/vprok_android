# Simple Weather

![](example.gif)

​
Приложение для простого и быстрого просмотра прогноза погоды. Оно позволяет сконцентрироваться на главном - информации о погоде, не отвлекая Вас ненужными мелочами или назойливой рекламой.

Simple Weather - ничего лишнего!

## Функции:
- Локальная база данных городов для всех стран мира ([openweathermap.org](https://openweathermap.org))
- Локальная база инициализируется при первом запуске приложения, процесс индицируется на экране заставки.
- По умолчанию для отслеживания выбраны Москва и Минск.
- При открытии приложения отображается список отслеживаемых городов с информацией о текущей погоде для каждого из них.
- Нажатие на город -> просмотр прогноза погоды на неделю для него.
- Свайп города влево -> удаление города из списка по нажатию на иконку корзины.
- Кнопка поиска в тулбаре -> переход на экран добавления города. Доступен текстовый поиск по именам городов в базе данных, а также определение ближайшего города по координатам пользователя.
- Кнопка информации в тулбаре -> переход на экран "О программе", включающий версию приложения, описание и кнопку обратной связи. 
- Все полученные данные о погоде сохраняются в локальную базу данных. Обновление данных происходит при загрузке списка городов, но не чаще раза в 1 час.
​
## Что использовалось​
- [OpenWeatherMap](https://openweathermap.org) - cервис для данных о погоде
- [Kotlin](https://kotlinlang.org/)
- [AndroidX](https://developer.android.com/jetpack/androidx)
- [Material Design 2.0](https://material.io/)
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
- [Koin](https://github.com/InsertKoinIO/koin)
- [Moxy](https://github.com/moxy-community/Moxy)
- [Cicerone](https://github.com/terrakok/Cicerone)
- [Retrofit](https://github.com/square/retrofit)
- [OkHttp](https://github.com/square/okhttp)
- [Room](https://developer.android.com/jetpack/androidx/releases/room)
- [Gson](https://github.com/google/gson)
- [Dexter​](https://github.com/Karumi/Dexter)
- [FastAdapter](https://github.com/mikepenz/FastAdapter)
​
## Структура проекта
Проект следует основам [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). Он разбит на следующие модули:

### app
Является основным модулем приложения и содержит категории *UI* и *Presenters* схемы *Clean Architecture*. Для организации презентационного слоя используется шаблон *Model-View-Presenter (MVP)*.
​
### data
Cодержит категории *Repositories*, *DB* и *Web* схемы *Clean Architecture*, то есть в нем содержится работа с сетью, описание локальной базы данных и различные *Gateway*.
​
### domain​
Cодержит категории *Use Cases* и *Entities* схемы *Clean Architecture*, то есть описание бизнес-логики и классы данных.

### resources
Cодержит часть ресурсов приложения, которая может использоваться в нескольких других модулях сразу (а не только в *app*). В частности, это *drawable*, *colors* и *strings*.
​
## Сборка проекта
Для успешной сборки проекта требуется выполнить следующие шаги:
- Установить среду разработки [Android Studio](https://developer.android.com/studio) версии 4.1 или новее.
- Импортировать проект из данного репозитория в Android Studio (*File -> Open...*).
- Синхронизировать Gradle для обновления всех зависимостей от сторонних библиотек (*File -> Sync Project with Gradle Files*).
- Открыть меню сборки проекта (*Build -> Generate Signed Bundle / APK*).
- Указать путь к ключевому файлу (*<папка проекта>/weather.jks*).
- Указать пароли для ключевого файла и ключа (см. константы *PROJECT_KEYSTORE_PASSWORD* и *PROJECT_KEY_PASSWORD* в файле *gradle.properties*).
- Выбрать тип сборки (для конечной сборки пользователям - *release*)
- Для сборки APK - включить подписи V1 и V2 (чекбоксы в нижней части окна).
- Нажать на кнопку *Finish* и дождаться завершения сборки приложения.
