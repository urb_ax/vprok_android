package com.appcraft.weather.data.network.model

import com.google.gson.annotations.SerializedName

data class CoordinatesModel(
    @SerializedName("lon") val lng: Double,
    @SerializedName("lat") val lat: Double
)