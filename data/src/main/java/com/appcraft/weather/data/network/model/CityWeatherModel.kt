package com.appcraft.weather.data.network.model

import com.google.gson.annotations.SerializedName

data class CityWeatherModel(
    @SerializedName("lat") val lat: Double,
    @SerializedName("lon") val lng: Double,
    @SerializedName("current") val current: CurrentWeatherForecastModel,
    @SerializedName("daily") val daily: List<WeatherForecastModel>
)