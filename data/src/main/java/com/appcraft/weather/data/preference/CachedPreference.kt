package com.appcraft.weather.data.preference

import com.f2prateek.rx.preferences2.Preference

class CachedPreference<T>(
    val preference: Preference<T>
) {
    private var cacheValue: T? = null
    private var cacheStatus: Boolean? = null

    @Suppress("unused")
    fun get(): T {
        if (cacheValue == null) {
            cacheValue = preference.get()
        }
        return cacheValue!!
    }

    @Suppress("unused")
    fun set(newValue: T) {
        preference.set(newValue)
        cacheValue = newValue
        cacheStatus = true
    }

    @Suppress("unused")
    fun isSet(): Boolean {
        if (cacheStatus == null) {
            cacheStatus = preference.isSet
        }
        return cacheStatus!!
    }

    @Suppress("unused")
    fun delete() {
        preference.delete()
        cacheValue = null
        cacheStatus = false
    }
}
