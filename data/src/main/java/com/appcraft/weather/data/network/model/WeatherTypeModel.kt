package com.appcraft.weather.data.network.model

import com.google.gson.annotations.SerializedName

data class WeatherTypeModel(
    @SerializedName("id") val id: Int,
    @SerializedName("main") val header: String,
    @SerializedName("description") val description: String,
    @SerializedName("icon") val iconType: String
)