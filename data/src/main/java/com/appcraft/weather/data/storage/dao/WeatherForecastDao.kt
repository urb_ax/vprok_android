package com.appcraft.weather.data.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appcraft.weather.data.storage.entity.WeatherForecastEntity

@Dao
interface WeatherForecastDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(entity: WeatherForecastEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAll(entities: List<WeatherForecastEntity>)

    @Query("SELECT * FROM WeatherForecastEntity WHERE cityId == :cityId")
    suspend fun getForecasts(cityId: Long): List<WeatherForecastEntity>

    @Query("DELETE FROM WeatherForecastEntity WHERE timestamp < :borderTimestamp")
    suspend fun deleteOldForecasts(borderTimestamp: Long)

    @Query("DELETE FROM WeatherForecastEntity WHERE cityId == :cityId")
    suspend fun deleteForecasts(cityId: Long)
}